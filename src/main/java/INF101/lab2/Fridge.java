package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {
    ArrayList<FridgeItem> inventory = new ArrayList<FridgeItem>();
    
    int max_Size = 20;
    @Override    //override betyr at metoden er definert i en annen fil og implementert her.
    public int totalSize() {
        return max_Size;
    }

    @Override
    public int nItemsInFridge() {
        return inventory.size();
    }
    
    @Override
    public boolean placeIn(FridgeItem item) {
        if (inventory.size()<max_Size) {
            inventory.add(item);
            return true;
        }
        else {
            
            return false;
        }
    }
    
    @Override
    public void takeOut(FridgeItem item) {
        if (inventory.contains(item)) {
            inventory.remove(item);
        }
        else {
            throw new NoSuchElementException("fridge does not contain " + item);
        }
        
        
    }
    
    @Override
    public void emptyFridge() {
        inventory.clear();
        
    }
    
    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expired_items = new ArrayList<FridgeItem>();
        for (FridgeItem items : inventory) {
            if (items.hasExpired()) {
                expired_items.add(items);
            }
        }
        for (FridgeItem items : expired_items) {
            inventory.remove(items);
        }
        return expired_items;
    }
}
